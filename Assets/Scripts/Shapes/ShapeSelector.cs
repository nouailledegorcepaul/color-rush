using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ShapeSelector : MonoBehaviour
{
    private Vector3 initPos;
    public Vector3 chosenPos;
    public bool chosen;
    public static GameObject ChosenGo;
    public static Mesh ChosenMesh;
    
    private void Start()
    {
        initPos = transform.localPosition;
    }

    private void Update()
    {
        if (chosen)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition,chosenPos, Time.deltaTime * 8f);
        }
        else
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition,initPos, Time.deltaTime * 8f);
        }
    }
    
    private void OnMouseDown()
    {
        GameManager.Instance.playerMesh = Instantiate(GetComponent<MeshFilter>().mesh);
        GameManager.Instance.player.GetComponent<MeshFilter>().mesh = GameManager.Instance.playerMesh;
        if (ChosenGo != gameObject)
        {
            chosen = true;
            if (!ChosenGo.IsUnityNull())
            {
                ChosenGo.GetComponent<ShapeSelector>().chosen = false;
            }
            ChosenGo = gameObject;
            ChosenMesh = Instantiate(GetComponent<MeshFilter>().mesh);
            MatAssigner[] assigners = FindObjectsOfType<MatAssigner>();
            foreach (MatAssigner mat in assigners)
            {
                mat.GetComponent<MeshFilter>().mesh = ChosenMesh;
            }
        }
    }
}
