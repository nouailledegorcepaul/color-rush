using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropletCollider : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            SoundManager.Instance.PlayDroplet();
            GameManager.Instance.AddDropletToCounter();
            Destroy(gameObject); 
        }
    }
}
