using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropletColor : MonoBehaviour
{
    // Start is called before the first frame update
    public Material mat;
    void Start()
    {
        gameObject.GetComponent<Renderer>().material = Instantiate(mat);
        gameObject.GetComponent<Renderer>().material.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
    }
}
