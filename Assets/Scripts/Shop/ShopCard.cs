using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Button = UnityEngine.UI.Button;
using Image = UnityEngine.UI.Image;

public class ShopCard : MonoBehaviour
{
    public Button button;
    public Text text;
    public Material mat;
    public Sprite green, red;
    public int price;
    public bool bought;
    public GameObject current;
    // Start is called before the first frame update
    void Start()
    {
        button.onClick.AddListener(Buy);
    }

    // Update is called once per frame
    void Update()
    {
        current = ColorPaletteController.pickedGo;
        if (!bought)
        {
            text.text = price.ToString();
            if (DropletManager.droplets >= price)
            {
                button.interactable = true;
                button.image.sprite = green;
            }
            else
            {
                button.interactable = false;
                button.image.sprite = red;
            }
        }
    }

    public void Buy()
    {
        bought = true;
        text.text = "Apply";
        button.onClick.RemoveListener(Buy);
        button.onClick.AddListener(Choose);
        Choose();
    }

    public void Choose()
    {
        if (!ColorPaletteController.pickedGo.IsUnityNull())
        {
            ColorState currentState = ColorPaletteController.pickedGo.GetComponent<MatSwitch>().state;
            if (mat == ColorPaletteController.pickedGo.GetComponent<Renderer>().sharedMaterial)
            {
                GameManager.CustomMaterials[currentState] = null;
                ColorPaletteController.pickedGo.GetComponent<Renderer>().material = GameManager.Materials[3];
                ColorPaletteController.pickedGo.GetComponent<Renderer>().material.color = GameManager.BaseColors[currentState];
            }
            else
            {
                GameManager.CustomMaterials[currentState] = mat;
                ColorPaletteController.pickedGo.GetComponent<Renderer>().material = mat; 
            }
        }
    }
}
