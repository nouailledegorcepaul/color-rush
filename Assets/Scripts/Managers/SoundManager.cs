﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    public AudioSource music, sound;
    public Slider musicSlider, soundSlider;
    public AudioClip droplet;
    public AudioClip ambianceMusic;
    public static SoundManager Instance;

    private void Awake()
    {
        if (Instance.IsUnityNull())
        {
            Instance = this;
            musicSlider.value = 1f;
            soundSlider.value = 1f;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        PlayMusic(ambianceMusic);
    }

    public void PlaySound(AudioClip originalClip)
    {
        sound.PlayOneShot(originalClip, sound.volume);
    }
    
    public void PlayDroplet(){
        PlaySound(droplet);
    }
    
    public void PlayMusic(AudioClip ac)
    {
        music.loop = true;
        music.clip = ac;
        music.Play(); 
    }

    public void StopMusic()
    {
        music.loop = false;
        music.Stop();
    }
    
    public void SetMusicVolume()
    {
        music.volume = musicSlider.value;
    }
    
    public void SetSoundVolume()
    {
        sound.volume = soundSlider.value;
    }
}