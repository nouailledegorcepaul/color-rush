using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class MeterCounter : MonoBehaviour
{
    public Transform player;
    private Vector3 startPoint;
    public int distance;
    public TextMeshProUGUI score;
    // Start is called before the first frame update
    void Start()
    {
        startPoint = player.position;
        distance = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (!player.IsUnityNull())
        {
            int current = (int) (player.position.z - startPoint.z);
            if (current > distance)
            {
                distance = current;
                score.text = distance.ToString();
            }
        }
    }

    public void ResetPlayer(Transform t)
    {
        distance = 0;
        player = t;
    }
}
