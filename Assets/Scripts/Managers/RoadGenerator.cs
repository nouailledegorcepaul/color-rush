using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using Outline = cakeslice.Outline;
using Random = UnityEngine.Random;

public class RoadGenerator : MonoBehaviour
{
    public Transform allRoads;
    public Transform allDroplets;
    public GameObject[] sections;
    public GameObject deathCollider;
    public GameObject droplet;
    public static bool CanGenerate;
    private GameObject lastGenerated;
    private int generated;
    // Start is called before the first frame update
    void Start()
    {
        lastGenerated = GameObject.FindWithTag("startRoad");
        CanGenerate = false;
        generated = 0;
    }

    private void Update()
    {
        if (CanGenerate)
        {
            if (lastGenerated.IsUnityNull())
            {
                lastGenerated = GameObject.FindWithTag("startRoad");
            }
            if (Random.Range(0, 100) < generated)
            {
                CanGenerate = false;
                GenerateRoad(true);
                generated = 0;
            }
            else
            {
                GenerateRoad();
                generated++;
            }
        }
        //
        // if (allRoads.transform.childCount > 2)
        // {
        //     Transform t = allRoads.GetChild(1);
        //     if (!t.GetComponent<RoadInit>().IsUnityNull())
        //     {
        //         foreach (GameObject g in t.GetComponent<RoadInit>().roads)
        //         {
        //             if (!g.IsUnityNull())
        //             {
        //                 if (!g.GetComponent<Outline>().IsUnityNull())
        //                 {
        //                     g.GetComponent<Outline>().enabled = false;
        //                 } 
        //             }
        //         }
        //     }
        //     t = allRoads.GetChild(2);
        //     if (!t.GetComponent<RoadInit>().IsUnityNull())
        //     {
        //         foreach (GameObject g in t.GetComponent<RoadInit>().roads)
        //         {
        //             if (!g.GetComponent<Outline>().IsUnityNull())
        //             {
        //                 g.GetComponent<Outline>().enabled = true;
        //             }
        //         } 
        //     }
        // }
    }
    
    public void GenerateRoad(bool isSwitch = false)
    {
        Transform endpoint = lastGenerated.transform.Find("EndPoint");
        GameObject nextRoad = sections[0]; // Prefab of color switch platform
        if (!isSwitch)
        {
            nextRoad = sections[Random.Range(1, sections.Length)];
        }
        nextRoad = Instantiate(nextRoad, endpoint.position, Quaternion.identity);
        float rotation = 0f;
        List<GameObject> droplets = new List<GameObject>();
        if (!isSwitch)
        {
            float scale = Random.Range(1.5f, 10f);
            nextRoad.transform.localScale = new Vector3(1, 1, scale);
            droplets = GenerateDroplets(nextRoad);
            if (Random.Range(0f, 1f) < 0.9f) // 20% chances to generate a rotated road
            {
                rotation = Random.Range(0f, 30f) - 20f;
                nextRoad.transform.Rotate(rotation, 0, 0, Space.World);
            }
        }
        Vector3 nextStartPoint = nextRoad.transform.Find("StartPoint").position;
        Vector3 nextEndPoint = nextRoad.transform.Find("EndPoint").position;
        float length = nextEndPoint.z - nextStartPoint.z;
        float height = nextEndPoint.y - nextStartPoint.y;
        
        float spacing = Random.Range(3f, 5f);
        int altitude = Random.Range(0, 3);
        float yPos = 0f;
        if (altitude == 0)
        {
            yPos = -Random.Range(2f, 6f);
        }
        else if (altitude == 2)
        {
            yPos = Random.Range(1f, 2f);
            spacing = 2f;
            if (rotation > 0)
            {
                yPos -= 0.7f;
            }
        }
        Vector3 nextRoadPos = new Vector3(endpoint.position.x, nextStartPoint.y + yPos + height / 2, endpoint.position.z + length/2 + spacing);
        nextRoad.transform.position = nextRoadPos;
        nextRoad.transform.SetParent(allRoads);
        GameObject go = GenerateDeathCollider(endpoint, nextRoad.transform.Find("StartPoint"));
        go.transform.SetParent(nextRoad.transform);
        // if (isSwitch)
        // {
        //     CalculateParabola(endpoint.position, nextRoad.transform.Find("StartPoint").position);
        // }
        lastGenerated = nextRoad;
        foreach (GameObject d in droplets)
        {
            d.transform.SetParent(allDroplets, true);
            d.transform.rotation = Quaternion.identity;
        }
    }

    public GameObject GenerateDeathCollider(Transform start, Transform end)
    {
        GameObject death = Instantiate(deathCollider, end.position, Quaternion.identity);
        double distance = Vector3.Distance(start.position, end.position) - 1;
        death.transform.localScale = new Vector3(1, 1, (float)distance);
        Vector3 pos = new Vector3(start.position.x, start.position.y + (end.position.y - start.position.y) / 2 - 1,
            start.position.z + (end.position.z - start.position.z) / 2);
        death.transform.position = pos;
        death.transform.rotation = Quaternion.LookRotation(new Vector3(0, end.position.y, end.position.z) -
                                                           new Vector3(0, start.position.y, start.position.z));
        return death;
    }
    
    public void GenerateStartRoad(GameObject startRoad, Vector3 startRoadPos)
    {
        GameObject road = Instantiate(startRoad, startRoadPos, Quaternion.identity);
        road.transform.SetParent(allRoads);
    }

    public List<GameObject> GenerateDroplets(GameObject baseRoad)
    {
        float length = baseRoad.transform.localScale.z;
        PlatformColor[] roads = baseRoad.GetComponentsInChildren<PlatformColor>();
        ColorState[] colors = new ColorState[3];
        List<GameObject> correctGo = new List<GameObject>();
        int correctColors = 0;
        for (int i = 0; i < roads.Length; i++)
        {
            colors[i] = roads[i].color;
            if (colors[i] == ColorState.WHITE || colors[i] == PlayerState.State || PlayerState.State == ColorState.GOLDEN)
            {
                correctColors++;
                correctGo.Add(roads[i].gameObject);
            }
        }
        Vector3 dropSize = droplet.GetComponent<Renderer>().bounds.size;
        int dropletAmount = (int)length - 1;
        float extra = length - dropletAmount * dropSize.z;
        extra /= dropletAmount;
        List<GameObject> droplets = new List<GameObject>();
        if (correctColors > 1)
        {
            GameObject road = correctGo[Random.Range(0, correctGo.Count)];
            Vector3 pos = road.transform.position;
            pos.y += 1;
            pos.z = road.transform.position.z - length / 2f + dropSize.z + extra;
            for (int i = 0; i < dropletAmount; i++)
            {
                GameObject drop = Instantiate(droplet, pos, Quaternion.identity);
                drop.transform.SetParent(baseRoad.transform);
                pos.z += dropSize.z * 1.5f;
                droplets.Add(drop);
            }
        }
        return droplets;
    }

    // y = x tan θ − gx2/2v2 cos2 θ
    // where,
    // y is the horizontal component,
    // x is the vertical component,
    //
    // θ is the angle at which projectile is thrown from the horizontal,
    // g is a constant called the acceleration due to gravity,
    // v is the initial velocity of projectile.
    public List<Vector3> CalculateParabola(Vector3 start, Vector3 end)
    {
        // start.y += 1;
        // end.y += 1;
        // float theta = Vector2.Angle(Vector2.right, new Vector2(0, 0.7f));
        // float g = 9.8f;
        // float v = 7f;
        //
        List<Vector3> res = new List<Vector3>();
        // for (float i = 0; i < Math.Abs(end.z - start.z); i +=  Math.Abs(end.z - start.z)/3)
        // {
        //     // pos = y
        //     // i = x
        //     // into 
        //     // pos = x
        //     // i = y
        //     // double i = x * Math.Tan(theta) - g * Math.Pow(x, 2) / 2 * Math.Pow(v, 2) * Math.Pow(Math.Cos(theta), 2);
        //     // double pos = i * Math.Tan(theta) - g * Math.Pow(i, 2) / 2 * Math.Pow(v, 2) * Math.Pow(Math.Cos(theta), 2);
        //     // double x = i * Math.Tan(theta) - g * Math.Pow(i, 2) / 2 * Math.Pow(v, 2) * Math.Pow(Math.Cos(theta), 2);
        //     // Vector3 newPos = new Vector3(start.x, (float) pos, start.z + i);
        //     // print(newPos);
        //     // res.Add(newPos);
        //     print(MathParabola.Parabola(start, end, i, 10));
        // }
        // print("----");
        return res;
    }
    
    public void EmptyRoads()
    {
        foreach (Transform road in allRoads)
        {
            Destroy(road.gameObject);
        }
    }
    
    public void EmptyDroplets()
    {
        foreach (Transform d in allDroplets)
        {
            Destroy(d.gameObject);
        }
    }
}
