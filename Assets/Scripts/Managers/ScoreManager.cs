using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public MeterCounter score;
    public GameObject scoreUI;
    public TextMeshProUGUI scoreText, bestScoreText;

    public void ShowScore()
    {
        scoreUI.SetActive(true);
        scoreText.text = score.distance.ToString();
        if (score.distance > GameManager.BestScore)
        {
            GameManager.BestScore = score.distance;
            bestScoreText.text = GameManager.BestScore + "    (NEW!)";
        }
        else
        {
            bestScoreText.text = GameManager.BestScore.ToString();
        }
    }

    public void HideScore()
    {
        scoreUI.SetActive(false);
    }
}
