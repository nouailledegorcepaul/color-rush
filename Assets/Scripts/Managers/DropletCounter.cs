using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class DropletCounter : MonoBehaviour
{
    public int droplet;
    public TextMeshProUGUI score;
    // Start is called before the first frame update
    
    public void InitCounter()
    {
        droplet = 0;
        score.text = droplet.ToString();
    }
    
    public void UpdateDroplet()
    {
        droplet++;
        score.text = droplet.ToString();
    }

    public void AddDropletsToTotal()
    {
        DropletManager.droplets += droplet;
        GameManager.Instance.Restart();
    }
}