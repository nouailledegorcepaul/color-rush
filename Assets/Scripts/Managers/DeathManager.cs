using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class DeathManager : MonoBehaviour
{
    private float time;
    private int times;
    private GameObject player;
    // Start is called before the first frame update
    // Update is called once per frame
    void Update()
    {
        if (!player.IsUnityNull())
        {
            time += Time.deltaTime;
            if (time > 0.5f)
            {
                player.SetActive(!player.activeSelf);
                time = 0;
                times++;
            }
            if (times == 4)
            {
                GameManager.Instance.scoreManager.ShowScore();
                Destroy(player.gameObject);
            }
        }
    }

    public void Lose(GameObject p)
    {
        player = p;
        time = 0;
        times = 0;
        player.SetActive(false);
    }
}
