using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyboxManager : MonoBehaviour
{
    private bool dark;

    public Material lightMat, darkMat;
    private Color lightColor, darkColor;
    public Light skyLight;

    private void Start()
    {
        lightColor = new Color(255/255f, 240/255f, 200/255f, 1f);
        darkColor = new Color(183/255f, 183/255f, 183/255f, 1f);
    }

    private void Update()
    {
        RenderSettings.skybox.SetFloat("_Rotation", 2f * Time.unscaledTime);
    }

    public void Switch()
    {
        dark = !dark;
        if (dark)
        {
            RenderSettings.skybox = darkMat;
            skyLight.color = darkColor;
        }
        else
        {
            RenderSettings.skybox = lightMat;
            skyLight.color = lightColor;
        }
    }
}
