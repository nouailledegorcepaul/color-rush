using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ColorCustom : MonoBehaviour
{
    public Vector3 basePos;
    public Vector3 customPos;
    public Vector3 shopPos;
    public bool customising;
    public bool shopping;
    
    void Update()
    {
        if (customising && transform.position != customPos)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition,customPos, Time.deltaTime * 8f);
        }
        else if (shopping && transform.position != shopPos)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition,shopPos, Time.deltaTime * 8f);
        }
        else if (!customising && !shopping && transform.position != basePos)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition,basePos, Time.deltaTime * 8f);
        }
    }
    

    public void Customize()
    {
        customising = true;
        int i = 0;
        foreach (MatSwitch t in transform.GetComponentsInChildren<MatSwitch>())
        {
            t.state = GameManager.Colors[i];
            t.newMaterial = GameManager.Materials[i];
            i++;
            t.GetComponent<Renderer>().material = t.newMaterial;
        }
    }
    
    public void Shop()
    {
        shopping = true;
        int i = 0;
        foreach (MatSwitch t in transform.GetComponentsInChildren<MatSwitch>())
        {
            t.state = GameManager.Colors[i];
            Material mat = GameManager.CustomMaterials[t.state];
            if (mat.IsUnityNull())
            {
                t.GetComponent<Renderer>().material.color = GameManager.Instance.RealColors[t.state];
            }
            else
            {
                t.newMaterial = mat;
                t.GetComponent<Renderer>().material = t.newMaterial;
            }
            
            i++;
        }
    }

    public bool Confirm()
    {
        if (!ColorPaletteController.pickedGo.IsUnityNull())
        {
            ColorPaletteController.pickedGo.GetComponent<MatSwitch>().chosen = false;
            ColorPaletteController.pickedGo = null;
            return false;
        }
        customising = false;
        shopping = false;
        foreach (Transform t in transform)
        {
            t.GetComponent<MatSwitch>().Refresh();
        }
        return true;
    }
    
    public void ResetColors()
    {
        GameManager.Instance.RealColors[ColorState.RED] = GameManager.BaseColors[ColorState.RED];
        GameManager.Instance.RealColors[ColorState.GREEN] = GameManager.BaseColors[ColorState.GREEN];
        GameManager.Instance.RealColors[ColorState.BLUE] = GameManager.BaseColors[ColorState.BLUE];
        foreach (MatSwitch t in transform.GetComponentsInChildren<MatSwitch>())
        {
            t.GetComponent<Renderer>().material.color = GameManager.BaseColors[t.GetComponent<MatSwitch>().state];
        }
    }
    
    public void ResetCustomMaterial()
    {
        foreach (MatSwitch t in transform.GetComponentsInChildren<MatSwitch>())
        {
            ColorState colorState = t.GetComponent<MatSwitch>().state;
            GameManager.CustomMaterials[colorState] = null;
            t.GetComponent<Renderer>().material.color = GameManager.BaseColors[colorState];
        }
    }
}
