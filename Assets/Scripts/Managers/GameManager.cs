using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public enum ColorState
{
    RED, GREEN, BLUE, WHITE, GOLDEN
}

public enum GameState
{
    PAUSE, PLAY
}

public enum MenuState
{
    MENU, CUSTOM, INGAME
}

public class GameManager : MonoBehaviour
{
    public static ColorState[] Colors =
    {
        ColorState.RED, ColorState.GREEN, ColorState.BLUE, ColorState.WHITE
    };

    public GameObject cam;
    public GameObject player;
    public Mesh playerMesh;
    public Vector3 startRoadPos;
    public Vector3 startPlayerPos;
    public GameObject startRoad;
    public GameObject playerPrefab;
    public GameObject mainMenuUI;
    public GameObject inGameUI;
    public GameObject colorUI;
    public GameObject settingsUI;
    public GameObject shopUI;
    public DeathManager deathManager;
    public RoadGenerator generator;
    public MeterCounter meterCounter;
    public DropletCounter dropletCounter;
    public ScoreManager scoreManager;
    public TextMeshProUGUI dropletText;
    public Material[] mats;
    public Material golden;
    // public Dictionary<ColorState, Color> customMats;
    public static int BestScore;
    public static Material[] Materials;
    public static Material GoldenMat;
    public static Dictionary<ColorState, Material> CustomMaterials;
    public static GameManager Instance;
    public static GameState State;
    public static MenuState MenuState;
    public static Dictionary<ColorState, Color> BaseColors;
    public Dictionary<ColorState, Color> RealColors;
    // Start is called before the first frame update
    private void Awake()
    {
        if (Instance is null)
        {
            Materials = mats;
            GoldenMat = golden;
            State = GameState.PAUSE;
            MenuState = MenuState.MENU;
            inGameUI.SetActive(false);
            colorUI.SetActive(false);
            
            CustomMaterials = new Dictionary<ColorState, Material>();
            CustomMaterials[ColorState.RED] = null;
            CustomMaterials[ColorState.GREEN] = null;
            CustomMaterials[ColorState.BLUE] = null;
            CustomMaterials[ColorState.WHITE] = null;
            BaseColors = new Dictionary<ColorState, Color>();
            RealColors = new Dictionary<ColorState, Color>();
            BaseColors[ColorState.RED] = HexToColor("C82828");
            BaseColors[ColorState.GREEN] = HexToColor("28C828");
            BaseColors[ColorState.BLUE] = HexToColor("2828C8");
            //realColors should contains saved colors from player
            RealColors[ColorState.RED] = BaseColors[ColorState.RED];
            RealColors[ColorState.GREEN] = BaseColors[ColorState.GREEN];
            RealColors[ColorState.BLUE] = BaseColors[ColorState.BLUE];
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (MenuState == MenuState.INGAME)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                Restart();
            }
        }
    }

    public void StartGame()
    {
        MenuState = MenuState.INGAME;
        Unpause();
        mainMenuUI.SetActive(false);
        inGameUI.SetActive(true);
        inGameUI.transform.Find("Pause").GetComponent<Button>().interactable = true;
    }

    public void Pause()
    {
        State = GameState.PAUSE;
        Time.timeScale = 0;
    }

    public void Unpause()
    {
        State = GameState.PLAY;
        Time.timeScale = 1;
    }
    
    public void Restart()
    {
        
        State = GameState.PAUSE;
        MenuState = MenuState.MENU;
        mainMenuUI.SetActive(true);
        scoreManager.HideScore();
        inGameUI.SetActive(false);
        generator.EmptyRoads();
        generator.EmptyDroplets();
        dropletText.text = DropletManager.droplets.ToString();
        if (!player.IsUnityNull())
        {
            Destroy(player);
        }
        player = Instantiate(playerPrefab, startPlayerPos, Quaternion.identity);
        meterCounter.ResetPlayer(player.transform);
        dropletCounter.InitCounter();
        generator.GenerateStartRoad(startRoad, startRoadPos);

    }

    public void PlayerLose()
    {
        inGameUI.transform.Find("Pause").GetComponent<Button>().interactable = false;
        Destroy(player.GetComponent<PlayerMovement>());
        Destroy(player.GetComponent<BoxCollider>());
        player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
        deathManager.Lose(player);
    }
    
    
    public void ResetColors()
    {
        GameObject custom = GameObject.FindWithTag("custom");
        if (!custom.IsUnityNull())
        {
            custom.GetComponent<ColorCustom>().ResetColors();
        }
    }
    
    public void ResetCustomMaterial()
    {
        GameObject custom = GameObject.FindWithTag("custom");
        if (!custom.IsUnityNull())
        {
            custom.GetComponent<ColorCustom>().ResetCustomMaterial();
        }
    }

    public void AddDropletToCounter()
    {
        dropletCounter.UpdateDroplet();
    }
    
    public void OnClickPause()
    {
        if (State == GameState.PAUSE)
        {
            Unpause();
        }
        else
        {
            Pause();
        }
    }

    public void OnClickColorPicker()
    {
        colorUI.SetActive(true);
        mainMenuUI.SetActive(false);
        MenuState = MenuState.CUSTOM;
        GameObject.FindWithTag("custom").GetComponent<ColorCustom>().Customize();
    }
    
    public void OnClickColorPickerConfirm()
    {
        bool done = GameObject.FindWithTag("custom").GetComponent<ColorCustom>().Confirm();
        if (done)
        {
            colorUI.SetActive(false);
            mainMenuUI.SetActive(true);
            MenuState = MenuState.MENU;
        }
    }

    public void OpenSettings()
    {
        settingsUI.SetActive(true);
    }
    
    public void CloseSettings()
    {
        settingsUI.SetActive(false);
    }
    
    public Color HexToColor(string hex)
    {
        string r = hex.Substring(0, 2);
        string g = hex.Substring(2, 2);
        string b = hex.Substring(4, 2);

        float r2 = System.Convert.ToInt32(r, 16);
        float g2 = System.Convert.ToInt32(g, 16);
        float b2 = System.Convert.ToInt32(b, 16);
        return new Color(r2/255f, g2/255f, b2/255f);
    }

    public void OnClickShop()
    {
        mainMenuUI.SetActive(false);
        shopUI.SetActive(true);
        cam.GetComponent<CameraShop>().Shop();
        MenuState = MenuState.CUSTOM;
        GameObject.FindWithTag("custom").GetComponent<ColorCustom>().Shop();
    }

    public void OnClickShopConfirm()
    {
        bool done = GameObject.FindWithTag("custom").GetComponent<ColorCustom>().Confirm();
        if (done)
        {
            mainMenuUI.SetActive(true);
            shopUI.SetActive(false);
            cam.GetComponent<CameraShop>().Leave();
        }
    }
}
