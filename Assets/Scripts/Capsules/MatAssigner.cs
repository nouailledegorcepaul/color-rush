using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MatAssigner : MonoBehaviour
{
    
    void Update()
    {
        if (GameManager.State == GameState.PLAY)
        {
            
            ColorState state;
            Material newMaterial;
            if (Random.Range(0f, 1f) < 0.05f)
            {
                state = ColorState.GOLDEN;
                newMaterial = GameManager.GoldenMat;
            }
            else
            {
                int rand = Random.Range(0, 3);
                state = GameManager.Colors[rand];
                newMaterial = GameManager.CustomMaterials[state];
                if (newMaterial.IsUnityNull())
                {
                    newMaterial = GameManager.Materials[rand];
                }   
                newMaterial.color = GameManager.Instance.RealColors[state];
            }
            
            GetComponent<Renderer>().material = newMaterial;
            GetComponent<MatSwitch>().SetAttributes(state, newMaterial);
            Destroy(this);
        }
    }
}
