using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MatChecker : MonoBehaviour
{
    public Material newMaterial;
    public ColorState state;
    
    private void Start()
    {
        int rand = Random.Range(0, 3);
        state = GameManager.Colors[rand];
        newMaterial = GameManager.Materials[rand];
    }

    private void Update()
    {
        if (GameManager.State == GameState.PLAY)
        {
            newMaterial.color = GameManager.Instance.RealColors[state];
            GetComponent<Renderer>().material = newMaterial;
            
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (state == PlayerState.State || PlayerState.State == ColorState.GOLDEN)
            {
                Destroy(transform.parent.gameObject);
            }
            else
            {
                GameManager.Instance.PlayerLose();
            }
        }
    }
}
