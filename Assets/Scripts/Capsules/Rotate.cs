using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public float rotateSpeed;

    // Update is called once per frame
    private void Update()
    {
        transform.Rotate(0, rotateSpeed, 0, Space.World);
    }

}
