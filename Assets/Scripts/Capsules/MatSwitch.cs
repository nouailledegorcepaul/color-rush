using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;

public class MatSwitch : MonoBehaviour
{
    public Material newMaterial;
    public ColorState state;
    public GameObject[] roads;
    public Vector3 initPos;
    public Vector3 chosenPos;
    public Vector3 shopChosenPos;
    public bool chosen;

    private void Start()
    {
        initPos = transform.localPosition;
        if (!ShapeSelector.ChosenMesh.IsUnityNull())
        {
            GetComponent<MeshFilter>().mesh = Instantiate(ShapeSelector.ChosenMesh);
        }
    }

    private void Update()
    {
        if (chosen)
        {
            if (transform.parent.GetComponent<ColorCustom>().customising)
            {
                transform.localPosition = Vector3.Lerp(transform.localPosition,chosenPos, Time.deltaTime * 8f);
            }
            if (transform.parent.GetComponent<ColorCustom>().shopping)
            {
                transform.localPosition = Vector3.Lerp(transform.localPosition,shopChosenPos, Time.deltaTime * 8f);
            }
        }
        else
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition,initPos, Time.deltaTime * 8f);
        }
    }

    private void OnMouseDown()
    {
        if (GameManager.MenuState == MenuState.CUSTOM)
        {
            if (chosen)
            {
                ColorPaletteController.pickedGo = null;
            }
            else
            {
                if (!ColorPaletteController.pickedGo.IsUnityNull())
                {
                    ColorPaletteController.pickedGo.GetComponent<MatSwitch>().chosen = false;
                }
                ColorPaletteController.pickedGo = gameObject;
            }
            chosen = !chosen;
        }
        
    }

    public void SetAttributes(ColorState colorState, Material material)
    {
        state = colorState;
        newMaterial = material;
    }
    
    public void Refresh()
    {
        GetComponent<Renderer>().material = GameManager.Materials[3];
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            PlayerState.State = state;
            other.GetComponent<Renderer>().material = newMaterial;
            other.GetComponent<Renderer>().material.mainTextureScale =
                new Vector2(1, 1);
            RoadGenerator.CanGenerate = true;
            Transform grandparent = transform.parent.transform.parent;
            foreach (GameObject go in roads)
            {
                go.GetComponent<Renderer>().material = newMaterial;
                go.GetComponent<Renderer>().material.mainTextureScale =
                    new Vector2(grandparent.localScale.x / 3, grandparent.localScale.z / 3);
            }
            Destroy(transform.parent.gameObject); // Destroy all Mesh Switch go
        }
    }
}
