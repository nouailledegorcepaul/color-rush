using System;
using System.Collections;
using System.Collections.Generic;
using cakeslice;
using Unity.VisualScripting;
using UnityEngine;

public class PlatformDetector : MonoBehaviour
{
    public GameObject ground;
    public Transform detector;

    private RaycastHit hit;
    // Update is called once per frame
    void Start()
    {
        ground = null;
    }

    void Update()
    {
        if (!ground.IsUnityNull())
        {
            Ray ray = new Ray(detector.position, new Vector3(0, -1f, 0f));
            if (Physics.Raycast(ray, out hit))
            {
                GameObject current = hit.transform.gameObject;
                if (current.CompareTag("terrain"))
                {
                
                    current = current.transform.parent.gameObject;
                    if (ground is not null && current != ground)
                    {
                        Fade(ground);
                    }
                    ground = current;
                    int nextIndex = current.transform.GetSiblingIndex() + 1;

                    Transform t = current.transform;
                    if (!t.GetComponent<RoadInit>().IsUnityNull())
                    {
                        foreach (GameObject g in t.GetComponent<RoadInit>().roads)
                        {
                            if (!g.IsUnityNull())
                            {
                                if (!g.GetComponent<Outline>().IsUnityNull())
                                {
                                    g.GetComponent<Outline>().enabled = false;
                                } 
                            }
                        }
                    }

                    t = current.transform.parent;
                    if (t.childCount > nextIndex)
                    {
                        t = t.GetChild(nextIndex);
                        if (!t.GetComponent<RoadInit>().IsUnityNull())
                        {
                            foreach (GameObject g in t.GetComponent<RoadInit>().roads)
                            {
                                if (!g.GetComponent<Outline>().IsUnityNull())
                                {
                                    g.GetComponent<Outline>().enabled = true;
                                }
                            } 
                        }  
                    }
                }
            }
        }
        else
        {
            GameObject g = GameObject.FindWithTag("startRoad");
            if (!g.IsUnityNull())
            {
                ground = g;
            }
        }
    }
    
    public void Fade(GameObject go)
    {
        foreach (PlatformFadeIn p in go.GetComponentsInChildren<PlatformFadeIn>())
        {
            p.enabled = true;
        }
        StartCoroutine(DestroyParent(go));
    }

    IEnumerator DestroyParent(GameObject go)
    {
        yield return new WaitForSeconds(1);
        Destroy(go);
    }
}
