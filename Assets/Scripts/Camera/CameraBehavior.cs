using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class CameraBehavior : MonoBehaviour
{
    public Transform player;
    public Transform detector;
    public Vector3 startPos;
    public Vector3 offset;
    private RaycastHit hit;
    
    // Update is called once per frame
    void Update()
    {
        if (!player.IsUnityNull())
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, player.position.z + offset.z);
            Ray rayCam = new Ray(detector.position, new Vector3(0, -1f, 0f));
            if (Physics.Raycast(rayCam, out hit))
            {
                GameObject current = hit.transform.gameObject;
                if (current.CompareTag("terrain"))
                {
                    float trueDistance = hit.distance - current.GetComponent<Renderer>().bounds.size.y / 2;
                    float extra = (offset.y - trueDistance);
                    Vector3 destination = new Vector3(transform.position.x, transform.position.y + extra,
                        transform.position.z);
                    transform.position = Vector3.Lerp(transform.position, destination, Time.deltaTime * 3f);
                }
            }
        }
        else
        {
            GameObject p = GameObject.FindWithTag("Player");
            if (p is not null)
            {
                transform.position = startPos;
                player = p.transform;
            }
        }
    }
}
