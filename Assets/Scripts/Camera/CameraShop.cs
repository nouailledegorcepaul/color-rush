using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShop : MonoBehaviour
{
    public bool shop;
    public float baseRotation;
    public float destRotation;
    // Update is called once per frame
    void Update()
    {
        if (shop)
        {
            GetComponent<CameraBehavior>().enabled = false;
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(destRotation,0,0), Time.deltaTime * 3f);
        }
        else
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(baseRotation,0,0), Time.deltaTime * 3f);
            if (Mathf.Abs(transform.localEulerAngles.x - baseRotation) < 3f)
            {
                GetComponent<CameraBehavior>().enabled = true;
            }
        }
    }

    public void Shop()
    {
        GetComponent<CameraBehavior>().enabled = false;
        shop = true;
    }

    public void Leave()
    {
        shop = false;
    }
}
