using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.UI;

public class PlayGamesServices : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] public Text debugText;
    [SerializeField] public TMPro.TMP_InputField leaderboard;
    void Start() {
        PlayGamesPlatform.Activate();
        PlayGamesPlatform.Instance.Authenticate(ProcessAuthentication);
    }

    internal void ProcessAuthentication(SignInStatus status) {
        if (status == SignInStatus.Success) {
            // Continue with Play Games Services
            print("Nice");
        } else {
            print(status);
            // Disable your integration with Play Games Services or show a login button
            // to ask users to sign-in. Clicking it should call
            // PlayGamesPlatform.Instance.ManuallyAuthenticate(ProcessAuthentication);
        }
        // debugText.text = "PlayGames initialized";
    }

    public void PostScoreLeaderboard()
    {
        PlayGamesPlatform.Instance.ReportScore(int.Parse(leaderboard.text), "CgkIgZXVmvkIEAIQAg", (bool success) =>
        {
            if (success)
            {
                debugText.text = "Success Leaderboard";
            }
            else
            {
                debugText.text = "Error Leaderboard";
            }
        });
    }
    
    public void AchivementCompleted()
    {
        PlayGamesPlatform.Instance.ReportProgress("CgkIgZXVmvkIEAIQAw", 100.0f, (bool success) =>
        {
            if (success)
            {
                debugText.text = "Success Achievement";
            }
            else
            {
                debugText.text = "Error Achievement";
            }
        });
    }
    
    public void ShowLeaderboard()
    {
        PlayGamesPlatform.Instance.ShowLeaderboardUI();
    }

    public void ShowAchievements()
    {
        PlayGamesPlatform.Instance.ShowAchievementsUI();
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
