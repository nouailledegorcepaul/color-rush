using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using Random = UnityEngine.Random;

public class RoadInit : MonoBehaviour
{
    public GameObject[] roads;
    // Start is called before the first frame update
    void Awake()
    {
        bool correctColor = false;
        foreach (GameObject road in roads)
        {
            int rand = Random.Range(0, 3);
            if (Random.Range(0f, 1f) < 0.05f)
            {
                rand = 3;
            }
            ColorState color = GameManager.Colors[rand];
            if (color == PlayerState.State || color == ColorState.WHITE || PlayerState.State == ColorState.GOLDEN)
            {
                correctColor = true;
            }
            road.GetComponent<PlatformColor>().color = color;
            Material customMat = GameManager.CustomMaterials[color];
            if (customMat.IsUnityNull())
            {
                road.GetComponent<Renderer>().material = Instantiate(GameManager.Materials[rand]);
            }
            else
            {
                road.GetComponent<Renderer>().material = Instantiate(customMat);
            }
        }

        if (!correctColor)
        {
            int playerColorIndex = Array.IndexOf(GameManager.Colors, PlayerState.State);
            int rand = Random.Range(0, roads.Length);
            roads[rand].GetComponent<PlatformColor>().color = PlayerState.State;
            Material customMat = GameManager.CustomMaterials[PlayerState.State];
            if (customMat.IsUnityNull())
            {
                roads[rand].GetComponent<Renderer>().material = Instantiate(GameManager.Materials[playerColorIndex]);
            }
            else
            {
                roads[rand].GetComponent<Renderer>().material = Instantiate(customMat);
            }
        }
    }

    private void Start()
    {
        foreach (GameObject road in roads)
        {
            road.GetComponent<Renderer>().material.mainTextureScale =
                new Vector2(transform.localScale.x / 3, transform.localScale.z / 3);
        }
    }
}
