using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlatformFadeIn : MonoBehaviour
{
    public Material transparent;
    private MeshRenderer mesh;
    private void Start()
    {
        GetComponent<Renderer>().material = transparent;
        mesh = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        Color c = mesh.material.color;
        if (c.a <= 0.01f)
        {
            Destroy(gameObject);
        }
        mesh.material.color = new Color(c.r, c.g, c.b, c.a - 0.02f);
    }
}
