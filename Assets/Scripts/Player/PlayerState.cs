using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState : MonoBehaviour
{
    public static ColorState State = ColorState.WHITE;

    private void Start()
    {
        GetComponent<MeshFilter>().mesh = GameManager.Instance.playerMesh;
    }
}
