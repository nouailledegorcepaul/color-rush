using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 5f;
    public float jumpHeight = 8f;
    public float slideDistance = 2f;
    public int positionIndex = 1;
    private float nextPosition;
    private float nextRotation;
    private Rigidbody _rigidbody;
    private Vector3 initialPos;
    private float distToGround;
    private RaycastHit hit;
    // Update is called once per frame

    private void Start()
    {
        nextPosition = transform.position.x;
        _rigidbody = GetComponent<Rigidbody>();
        distToGround = GetComponent<Collider>().bounds.extents.y;
    }

    void Update()
    {
        if (GameManager.State == GameState.PLAY)
        {
            transform.Translate(Vector3.forward * Time.deltaTime * speed, Space.World);
            if( Input.GetMouseButtonDown(0))
            {
                initialPos = Input.mousePosition;
            }
            if( Input.GetMouseButtonUp(0)){
                Calculate(Input.mousePosition);
            }
            if (Input.GetKeyDown(KeyCode.Z))
            {
                if (CanJump())
                {
                    _rigidbody.velocity = Vector3.up * jumpHeight;
                }
            }
            if (Input.GetKeyDown(KeyCode.S) && !IsGrounded())
            {
                _rigidbody.velocity = new Vector3(0, -30, 0);
            }
            if (Input.GetKeyDown(KeyCode.Q))
            {
                if (positionIndex > 0)
                {
                    positionIndex--;
                    nextPosition -= slideDistance;
                }
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                if (positionIndex < 2)
                {
                    positionIndex++;
                    nextPosition += slideDistance;
                }
            }
            transform.position = Vector3.Lerp(transform.position,new Vector3(nextPosition, transform.position.y, transform.position.z), Time.deltaTime * 15f);
            Ray ray = new Ray(transform.position, new Vector3(0, -1f, 0f));
            if (Physics.Raycast(ray, out hit))
            {
                GameObject current = hit.transform.gameObject;
                if (current.CompareTag("terrain"))
                {
                    nextRotation = current.transform.parent.localEulerAngles.x;
                    transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(nextRotation, 0, 0), Time.deltaTime * 5f);
                    if (IsGrounded() && current.GetComponent<PlatformColor>().color != PlayerState.State)
                    {
                        if (PlayerState.State == ColorState.GOLDEN)
                        {
                            current.GetComponent<Renderer>().material = GameManager.GoldenMat;
                        }
                        else if (current.GetComponent<PlatformColor>().color != ColorState.WHITE)
                        {
                            GameManager.Instance.PlayerLose();
                        }
                    }
                }
            }
            else
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.identity, Time.deltaTime * 5f);
            }
        }
    }
    
    bool IsGrounded()
    {
        return Physics.Raycast(transform.position, Vector3.down, distToGround + 0.2f);
    }
    
    bool CanJump()
    {
        return Physics.Raycast(transform.position, Vector3.down, distToGround + 0.5f);
    }
    
    void Calculate(Vector3 finalPos)
    {
        if (initialPos != Vector3.zero)
        {
            float disX = Mathf.Abs(initialPos.x - finalPos.x);
            float disY = Mathf.Abs(initialPos.y - finalPos.y);
            if(disX>0 || disY>0)
            {
                if (disX > disY) 
                {
                    if (initialPos.x > finalPos.x)
                    {
                        if (positionIndex > 0)
                        {
                            positionIndex--;
                            nextPosition -= slideDistance;
                        }
                    }
                    else
                    {
                        if (positionIndex < 2)
                        {
                            positionIndex++;
                            nextPosition += slideDistance;
                        }
                    }
                }
                else 
                {   
                    if (initialPos.y > finalPos.y )
                    {
                        if (!IsGrounded())
                        {
                            _rigidbody.velocity = new Vector3(0, -30, 0);
                        }
                    }
                    else
                    {
                        if (CanJump())
                        {
                            _rigidbody.velocity = Vector3.up * jumpHeight;
                        }
                    }
                }
            }
        }
    }
}
